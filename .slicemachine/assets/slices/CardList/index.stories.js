import MyComponent from '../../../../slices/Card';

export default {
  title: 'slices/CardList'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{"image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1515378791036-0648a3ef77b2"},"title":[{"type":"heading1","text":"Meant","spans":[]}],"description":[{"type":"paragraph","text":"Et amet proident aliquip ullamco sint eiusmod nostrud quis aliquip ipsum labore. Nostrud consequat fugiat aute enim eiusmod sunt ipsum ipsum esse. Eiusmod velit pariatur amet ad cupidatat aute sunt nostrud reprehenderit.","spans":[]}],"ctalink":{"link_type":"Web","url":"http://twitter.com"},"ctalabel":"tube"}],"primary":{},"slice_type":"card","id":"_Default"}} />
_Default.storyName = ''
