import MyComponent from '../../../../slices/Toto';

export default {
  title: 'slices/Toto'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Actual","spans":[]}],"description":[{"type":"paragraph","text":"Exercitation et magna pariatur aliquip. Adipisicing in minim esse non occaecat mollit dolor do elit culpa veniam occaecat dolore id.","spans":[]}],"toto":{"link_type":"Web","url":"http://twitter.com"}},"slice_type":"toto","id":"_Default"}} />
_Default.storyName = ''
