import MyComponent from '../../../../slices/BannerWithCta';

export default {
  title: 'slices/BannerWithCta'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Castle","spans":[]}],"description":[{"type":"paragraph","text":"Nisi qui cillum incididunt do sint reprehenderit anim veniam eu duis sit velit. Nisi laborum mollit ex dolor amet fugiat.","spans":[]}],"calltoaction":{"link_type":"Web","url":"https://prismic.io"},"labellink":"combine"},"slice_type":"banner_with_cta","id":"_Default"}} />
_Default.storyName = ''

export const _WithImage = () => <MyComponent slice={{"variation":"withImage","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Supply","spans":[]}],"description":[{"type":"paragraph","text":"Ex veniam elit Lorem nostrud irure excepteur consectetur. Sit reprehenderit duis sint aliquip commodo reprehenderit in voluptate id pariatur ea consequat fugiat in. Ea ex dolor non fugiat do laborum cillum.","spans":[]}],"calltoaction":{"link_type":"Web","url":"https://slicemachine.dev"},"labellink":"clay","image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2"}},"slice_type":"banner_with_cta","id":"_WithImage"}} />
_WithImage.storyName = ''
