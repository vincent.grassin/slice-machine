import MyComponent from '../../../../slices/Hero';

export default {
  title: 'slices/Hero'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1587653915936-5623ea0b949a"},"title":[{"type":"heading1","text":"Folks","spans":[]}],"description":[{"type":"paragraph","text":"Adipisicing ad culpa ullamco dolor dolor laborum magna ullamco labore proident qui Lorem sunt aute nostrud. Anim irure duis ea incididunt. Magna consequat sunt ea ea ut adipisicing dolor.","spans":[]}],"ctalink":{"link_type":"Web","url":"https://prismic.io"},"ctalabel":"inside"},"slice_type":"hero","id":"_Default"}} />
_Default.storyName = ''
