import React from 'react'
import { PrismicRichText, PrismicLink } from '@prismicio/react'
import { css } from '@emotion/react'
import { colors, font, layout, spacing } from '../../components/tokens'
import Button from '../../components/Button'
import Avatar from '../../components/Avatar'
import Container from '../../components/Container'

const styles = {
  title: css({
    letterSpacing: font.letterSpacing.title,
    fontSize: font.size.l,
    fontWeight: 'bold',
    color: colors.dark.prune,
    lineHeight: 1,
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    [layout.mq.m]: {
      display: 'inline-block',
      fontSize: font.size.xxxl,
    },
  }),
  button: css({
    marginTop: spacing.l,
  }),
}

const BannerWithCta = ({ slice }) => {
  return (
    <div
      css={
        slice.variation !== 'withImage'
          ? {
              backgroundColor: colors.neutral.almostWhite,
              paddingTop: spacing.xxl,
              paddingBottom: spacing.xxl,
            }
          : ''
      }
    >
      <Container noMargin={slice.variation !== 'withImage'} smallWidth>
        <section
          css={{
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center',
            margin: '0 auto',
          }}
        >
          <div>
            {slice.primary.image && (
              <Avatar
                src={slice.primary.image.url}
                alt={slice.primary.image.alt}
                small
              />
            )}
          </div>
          <div
            css={{
              display: 'flex',
              justifyContent: 'center',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            <span css={styles.title}>
              {slice.primary.title ? (
                <PrismicRichText field={slice.primary.title} />
              ) : (
                <h2>Template slice, update me!</h2>
              )}
            </span>
            {slice.primary.description ? (
              <PrismicRichText field={slice.primary.description} />
            ) : (
              <p>start by editing this slice from inside Slice Machine!</p>
            )}
            {!!slice.primary.calltoaction && (
              <div css={styles.button}>
                <PrismicLink field={slice.primary.calltoaction}>
                  <Button>{slice.primary.labellink}</Button>
                </PrismicLink>
              </div>
            )}
          </div>
        </section>
      </Container>
    </div>
  )
}

export default BannerWithCta
