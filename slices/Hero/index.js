import React from 'react'
import { PrismicLink, PrismicRichText } from '@prismicio/react'
import Avatar from '../../components/Avatar'
import Button from '../../components/Button'
import { css } from '@emotion/react'
import * as tokens from '../../components/tokens'
import { colors, font, layout, spacing } from '../../components/tokens'
import Container from '../../components/Container'

const styles = {
  hero: css({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    [layout.mq.m]: {
      overflow: 'hidden',
      flexDirection: 'row-reverse',
      position: 'relative',
      maxWidth: '100vw',
    },
    [layout.mq.xl]: {
      marginBottom: spacing['3xl'],
      maxWidth: layout.mq.hero,
      margin: '0 auto',
    },
    [layout.mq.hero]: {
      overflow: 'visible',
    },
  }),
  withPadding: css({
    [layout.mq.l]: {
      paddingTop: spacing['3xl'],
      paddingBottom: spacing['3xl'],
    },
  }),
  titleWrapper: css({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: spacing['3xl'],
    [layout.mq.m]: {
      maxWidth: '60%',
      alignItems: 'flex-start',
    },
  }),
  title: css({
    letterSpacing: font.letterSpacing.title,
    fontSize: font.size.l,
    fontWeight: 'bold',
    color: colors.dark.prune,
    lineHeight: 1,
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    [layout.mq.m]: {
      display: 'inline-block',
      fontSize: font.size.xxxl,
      textAlign: 'left',
    },
  }),
  button: css({
    marginTop: spacing.l,
  }),
  blobContainer: css({
    maxWidth: '100vw',
    padding: '1rem',
    [layout.mq.m]: {
      position: 'absolute',
      right: 0,
      zIndex: -1,
      width: '40%',
      height: '80%',
    },
  }),
  blob: css({
    width: '100vw',
    height: 'auto',
    [layout.mq.m]: {
      width: '100%',
      height: 'initial',
    },
  }),
  hashtag: css({
    color: tokens.colors.neutral.deepGray,
    textDecoration: 'underline',
    marginRight: tokens.spacing.m,
  }),
  logoWrapper: css({
    maxWidth: 200,
  }),
}

const Hero = ({ slice }) => (
  <section css={styles.hero}>
    {slice.primary.image ? (
      <div css={styles.blobContainer}>
        <Avatar src={slice.primary.image.url} alt={slice.primary.image.alt} />
      </div>
    ) : (
      <h2>Add image !</h2>
    )}
    <Container>
      <div css={styles.titleWrapper}>
        <span css={styles.title}>
          {slice.primary.title ? (
            <PrismicRichText field={slice.primary.title} />
          ) : (
            <h2>Template slice, update me!</h2>
          )}
        </span>
        {slice.primary.description ? (
          <PrismicRichText field={slice.primary.description} />
        ) : (
          <p>start by editing this slice from inside Slice Machine!</p>
        )}
        {!!slice.primary.ctalink && (
          <div css={styles.button}>
            <PrismicLink field={slice.primary.ctalink}>
              <Button>{slice.primary.ctalabel}</Button>
            </PrismicLink>
          </div>
        )}
      </div>
    </Container>
  </section>
)

export default Hero
