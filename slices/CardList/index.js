import { css } from '@emotion/react'
import { PrismicLink, PrismicRichText } from '@prismicio/react'
import React from 'react'
import Avatar from '../../components/Avatar'
import Button from '../../components/Button'
import Container from '../../components/Container'
import { colors, font, spacing } from './../../components/tokens'

const styles = {
  block: css({
    margin: '60px auto',
    backgroundColor: colors.lighter.grey,
    padding: '37px 28px',
    borderRadius: spacing.xs,
    fontSize: font.size.l,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    maxWidth: 350,
    '& > :not(:last-child)': {
      marginBottom: spacing.m,
    },
    '& p': {
      color: colors.light.prune,
    },
  }),
  title: css({
    color: colors.light.prune,
    letterSpacing: font.letterSpacing.title,
    fontSize: font.size.xl,
  }),
  description: css({
    textAlign: 'center',
  }),
  grid: css({
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 20,
  }),
}

const CardList = ({ slice }) => (
  <Container>
    <section css={styles.grid}>
      {slice?.items?.map((item, i) => (
        <div key={i} css={styles.block}>
          {item.image ? (
            <Avatar src={item.image.url} alt={item.image.alt} />
          ) : (
            <h2>Add image !</h2>
          )}
          <span css={styles.title}>
            {item.title ? (
              <PrismicRichText css={styles.title} field={item.title} />
            ) : (
              <h2>Template slice, update me!</h2>
            )}
          </span>
          {item.description ? (
            <div css={styles.description}>
              <PrismicRichText field={item.description} />
            </div>
          ) : (
            <p>start by editing this slice from inside Slice Machine!</p>
          )}
          {!!item.ctalink && (
            <PrismicLink field={item.ctalink}>
              <Button>{item.ctalabel}</Button>
            </PrismicLink>
          )}
        </div>
      ))}
    </section>
  </Container>
)

export default CardList
