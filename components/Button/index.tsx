import { css } from '@emotion/react'
import { FC, MouseEventHandler, ReactNode } from 'react'
import { colors, ColorVariants, font, spacing } from './../tokens'

export interface ButtonProps {
  color?: keyof typeof ColorVariants
  isExternal?: boolean
  small?: boolean
  ghost?: boolean
  subline?: boolean
  disabled?: boolean
  onClick?: MouseEventHandler<HTMLButtonElement>
  href?: string
  type?: 'button' | 'submit' | 'reset'
  children?: ReactNode
}

const PADDING_X = 25
const PADDING_X_SMALL = 20

const buttonStyle = {
  textDecoration: 'none',
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'center',
  cursor: 'pointer',
  border: 'none',
  color: colors.light.prune,
  backgroundColor: 'transparent',
  '&>*:not(:last-child)': {
    marginRight: spacing.s,
  },
}

const Button: FC<ButtonProps> = ({
  color,
  href,
  onClick = () => {},
  children,
  isExternal,
  small,
  disabled,
  type = 'button',
  subline,
  ...props
}) => {
  const styleSubline = css({
    '&:hover': {
      '&:after': {
        left: small ? PADDING_X : PADDING_X_SMALL,
      },
    },
    '&:after': {
      content: '""',
      width: small
        ? `calc(100% - ${PADDING_X_SMALL}px - ${PADDING_X_SMALL}px)`
        : `calc(100% - ${PADDING_X}px - ${PADDING_X}px)`,
      height: 3,
      transition: 'all 0.2s linear',
      backgroundColor: color ? colors.light[color] : colors.light.prune,
      borderRadius: 200,
      position: 'absolute',
      left: 0,
      bottom: 0,
    },
    '&:focus': {
      color: color ? colors.text[color] : colors.text.prune,
      '&:after': {
        left: small ? PADDING_X : PADDING_X_SMALL,
      },
    },
    '&:focus-visible': {
      color: color ? colors.text[color] : colors.text.prune,
      '&:after': {
        left: small ? PADDING_X : PADDING_X_SMALL,
      },
    },
  })

  const styleNotSubline = css({
    boxShadow: `0px 0px 0px 2px ${color ? colors.light[color] : colors.light.prune}`,
    transition: 'box-shadow 0.05s linear',
    margin: '0.5em',
    '&:hover': {
      boxShadow: `0px 0px 0px 3px ${color ? colors.light[color] : colors.light.prune}`,
    },
    '&:focus': {
      backgroundColor: color ? colors.text[color] : colors.text.prune,
      color: colors.neutral.white,
      path: {
        fill: colors.neutral.white,
      },
    },
    '&:focus-visible': {
      backgroundColor: color ? colors.text[color] : colors.text.prune,
      color: colors.neutral.white,
      path: {
        fill: colors.neutral.white,
      },
    },
  })

  const style = css({
    ...buttonStyle,
    fontSize: small ? font.size.s : font.size.m,
    fontWeight: 'bold',
    position: 'relative',
    borderRadius: subline ? 0 : 81,
    padding: small ? `8px ${PADDING_X_SMALL}px` : `12px ${PADDING_X}px`,
    '&:focus': {
      outline: 'none',
    },
    '&:disabled': {
      opacity: 0.2,
      pointerEvents: 'none',
    },
  })

  return (
    <button
      css={[style, subline ? styleSubline : styleNotSubline]}
      onClick={onClick}
      role="button"
      type={type}
      disabled={disabled}
    >
      {children}
    </button>
  )
}

export default Button
