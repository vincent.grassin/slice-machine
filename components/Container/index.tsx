import { css } from '@emotion/react'
import { FC, ReactNode } from 'react'
import { layout, spacing } from './../tokens'

export interface ContainerProps {
  fluid?: boolean
  smallWidth?: boolean
  noMargin?: boolean
  children?: ReactNode
}

const style = {
  container: css({
    maxWidth: '100%',
    margin: `0 auto`,
    padding: `0 ${spacing.m}`,
  }),
  containerSize: css({
    [layout.mq.l]: {
      width: '960px',
    },
    [layout.mq.xl]: {
      width: '1140px',
    },
  }),
  containerSmallSize: css({
    [layout.mq.l]: {
      width: '820px',
    },
  }),
  fluid: css({
    padding: 0,
  }),
  fluidWrapper: css({
    width: '100%',
  }),
  margin: css({
    marginBottom: spacing['4xl'],
  }),
}
const Container: FC<ContainerProps> = ({ children, fluid, smallWidth, noMargin, ...props }) => {
  const contentProps = fluid ? {} : props
  const Content = (
    <div
      css={[
        style.container,
        smallWidth ? style.containerSmallSize : style.containerSize,
        noMargin ? {} : style.margin,
        fluid && style.fluid,
      ]}
      {...contentProps}
    >
      {children}
    </div>
  )
  if (fluid) {
    return (
      <div css={[style.fluidWrapper]} {...props}>
        {Content}
      </div>
    )
  }

  return Content
}

export default Container
