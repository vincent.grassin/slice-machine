export const colors = {
    lighter: {
      white: '#fff',
      blue: 'rgba(95, 175, 226, 0.1)',
      green: 'rgba(51, 177, 143, 0.1)',
      red: 'rgba(226, 101, 102, 0.1)',
      orange: 'rgba(244, 161, 96, 0.1)',
      prune: 'rgba(61, 50, 76, 0.1)',
      mauve: 'rgba(151, 119, 168, 0.1)',
      grey: 'rgba(245, 246, 247, 0.6)',
    },
    light: {
      white: '#FFF',
      blue: '#5FAFE2',
      green: '#33B18F',
      red: '#E26566',
      orange: '#F4A160',
      prune: '#3D324C',
      mauve: '#9777A8',
    },
    dark: {
      white: '#FFF',
      blue: '#5891BC',
      green: '#399483',
      red: '#BE5A62',
      orange: '#CA8460',
      mauve: '#816691',
      prune: '#2F243F',
    },
    neutral: {
      white: '#FFF',
      almostWhite: '#F5F6F7',
      silver: '#DFE4EA',
      lightGray: '#BBC7D3',
      gray: '#8CA0B3',
      darkGray: '#6C7D8E',
      black: '#2E2E2E',
      deepGray: '#647383',
    },
    text: {
      orange: '#CA8460',
      blue: '#5891BC',
      green: '#399483',
      prune: '#3D324C',
      mauve: '#816691',
      red: '#BB575F',
      white: '#FFF',
    },
    bgColors: {
      orange: '#FCA664',
      mauve: '#C197D9',
      red: '#F67D7E',
      blue: '#5CB8F2',
      green: '#3BC6A0',
    },
  }
  
  export enum ColorContrast {
    neutral = 'neutral',
    light = 'light',
    dark = 'dark',
  }
  
  export enum ColorVariants {
    blue = 'blue',
    green = 'green',
    red = 'red',
    orange = 'orange',
    mauve = 'mauve',
    prune = 'prune',
    white = 'white',
  }
  
  export const baseFontSize = 16
  export const remCalc = (pixels: number) => {
    return `${pixels / baseFontSize}rem`
  }
  export const font = {
    family: {
      sans: 'Arial, Helvetica, sans-serif',
    },
    letterSpacing: {
      default: '0',
      title: '-0.05em',
    },
    size: {
      s: remCalc(14),
      m: remCalc(16),
      l: remCalc(24),
      xl: remCalc(32),
      xxl: remCalc(48),
      xxxl: remCalc(64),
    },
  }
  
  const baseSpacing = 4
  export const spacing = {
    /** 0px */
    0: '0',
    /** 1px */
    1: '1px',
    /** 4px */
    xs: `${baseSpacing * 1}px`, // 4px
    /** 8px */
    s: `${baseSpacing * 2}px`, // 8px
    /** 16px */
    m: `${baseSpacing * 4}px`, // 16px
    /** 24px */
    l: `${baseSpacing * 6}px`, // 24px
    /** 32px */
    xl: `${baseSpacing * 8}px`, // 32px
    /** 40px */
    xxl: `${baseSpacing * 10}px`, // 40px
    /** 80px */
    '3xl': `${baseSpacing * 20}px`, // 80px
    /** 120px */
    '4xl': `${baseSpacing * 30}px`, // 120px
    /** 160px */
    '5xl': `${baseSpacing * 40}px`, // 160px
    /** 520px */
    '6xl': `${baseSpacing * 130}px`, // 520px
  }
  
  const breakpoints = [576, 768, 992, 1200, 1440]
  const mediaqueries = breakpoints.map((breakpoint) => `@media (min-width: ${breakpoint}px)`)
  
  export const layout = {
    containerWidth: '1140px',
    mq: {
      s: mediaqueries[0],
      m: mediaqueries[1],
      l: mediaqueries[2],
      xl: mediaqueries[3],
      hero: mediaqueries[4],
    },
    breakpoints: {
      s: breakpoints[0],
      m: breakpoints[1],
      l: breakpoints[2],
      xl: breakpoints[3],
    },
  }
  