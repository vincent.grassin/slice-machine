import { FC } from 'react'
import Image from 'next/image'
import { css } from '@emotion/react'
import { colors } from '../tokens'

export interface AvatarProps {
  src: string
  alt: string
  small: boolean
}

const styles = {
  avatar: css({
    borderRadius: '100%',
    position: 'relative',
    overflow: 'hidden',
    border: `5px solid ${colors.light.prune}`,
  }),
  w500: css({
    height: 500,
    width: 500,
  }),
  w80: css({
    height: 80,
    width: 80,
  }),
}

const Avatar: FC<AvatarProps> = ({ src, alt = '', small = false, ...props }) => {
  return (
    <Image
      css={[styles.avatar, small ? styles.w80 : styles.w500]}
      src={src}
      alt={alt}
      objectFit="cover"
      quality={25}
      height={small ? 80 : 500}
      width={small ? 80 : 500}
      priority={true}
      {...props}
    />
  )
}

export default Avatar